import requests
import datetime


class ClientApplication:
    """
    A clientApplication stores configuration state for a session and allows
    you to authenticate the client against the Ninja API.

    :type urlBase: string
    :param urlBase: The base URL that is used to connect to the Ninja API
                    (ex. https://app.ninjarmm.com)
    :type grantType: string
    :param grantType: The type of request being made, either using a
                      client-side obtained authorization code or refresh 
                      token for the purpose of downscoping a token.
    :type clientId: string
    :param clientId: The Client ID of the application requesting an access
                     token.
    :type clientSecret: string
    :param clientSecret: The client secret of the application requesting an
                         access token.
    :type scope: tuple
    :param scope: A list of application scopes that you'd like to
                  authenticate for.
    """

    AUTH_ENDPOINT = '/ws/oauth/token'

    def __init__(self, urlBase, grantType, clientId, clientSecret, scope):
        self.urlBase = urlBase
        self.grantType = grantType
        self.clientId = clientId
        self.__clientSecret = clientSecret
        self.scope = scope

    def authenticate(self):
        """Authenticates the ClientApplication"""
        url = f"{self.urlBase}{self.AUTH_ENDPOINT}"
        payload = self.__generateAuthPayload()
        print(payload)
        headers = {"Content-Type": "application/x-www-form-urlencoded"}
        now = datetime.datetime.now()
        responseRaw = requests.post(url=url, data=payload, headers=headers)
        response = responseRaw.json()
        print(response)
        tokenLifetime = datetime.timedelta(
            seconds=response['expires_in'])
        response['expires_at'] = now + tokenLifetime
        self.accessToken = response['access_token']
        self.tokenExpiresAt = response['expires_at']

    def __generateAuthPayload(self):
        scopeStr = " ".join(self.scope)
        # return (f"grant_type={self.grantType}&client_id={self.clientId}"
        # f"&client_secret={self.__clientSecret}&scope={scopeStr}")
        return (f"grant_type={self.grantType}&client_id={self.clientId}"
                f"&client_secret={self.__clientSecret}&scope={scopeStr}")
